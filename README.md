### Backup and restore MOngodb:

mongodump --uri="mongodb://127.0.0.1:27177/sulradio" -d sulradio -o sulradio

mongorestore --uri="mongodb://127.0.0.1:27099/sulradio" --username="admin" --password="" --authenticationDatabase="admin" sulradio20240113.json


Pra restaurar backup do spatie/laravel-backup

Depois de extrair o arquivo execute

gunzip mongodb-sulradio.archive.gz

depois 

mongorestore --uri="mongodb://127.0.0.1:27017/sulradio" --username="admin" --password="" --authenticationDatabase="sulradio" --archive=mongodb-sulradio.archive


### Build image
docker build  --no-cache -t sulradio ./

docker tag sulradio diegoneumann/sulradio:latest

docker push diegoneumann/sulradio:latest

### Montando Ambiente
##### MongoDb
curl -fsSL https://pgp.mongodb.com/server-7.0.asc | sudo gpg -o /usr/share/keyrings/mongodb-server-7.0.gpg --dearmor

echo "deb [ arch=amd64,arm64 signed-by=/usr/share/keyrings/mongodb-server-7.0.gpg ] https://repo.mongodb.org/apt/ubuntu jammy/mongodb-org/7.0 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-7.0.list

sudo apt-get update

sudo apt-get install -y mongodb-org

sudo systemctl start mongod

sudo systemctl daemon-reload

sudo systemctl enable mongod

mongosh

use sulradio;

```bash
db.createUser({
      user: "admin",
      pwd: "8p67zRT57_hg",
      roles: [
      { role: "dbAdmin", db: "sulradio" },
      { role: "readWrite", db: "sulradio" },
      ]
    }
);
```

Edite as permissoes
sudo vim /etc/mongod.conf

net:
port: 27017
bindIp: 0.0.0.0

security:
authorization: enabled

Reinicie o mongo
sudo systemctl restart mongod

##### Instalando  mysql
sudo mysql

CREATE DATABASE sead_sulradio;

CREATE USER 'sulradio'@'%' IDENTIFIED BY 'OpLbA9r_dk4st';
GRANT ALL PRIVILEGES ON sead_sulradio.* TO 'sulradio'@'%' WITH GRANT OPTION;
FLUSH PRIVILEGES;


##### Portainer
sudo apt install -y apt-transport-https ca-certificates curl software-properties-common

curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"

sudo apt update

sudo apt install -y docker-ce

sudo usermod -aG docker ${USER}

sudo curl -L "https://github.com/docker/compose/releases/download/1.29.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose

sudo chmod +x /usr/local/bin/docker-compose



### Para IPV6
Crie o arquivo: \
sudo vim /etc/docker/daemon.json \ 

```bash
{
"ipv6": true,
"fixed-cidr-v6": "IPV6::/80"
}
```
sudo systemctl restart docker
docker swarm init --advertise-addr UPV6_ \
docker network create --driver=overlay --ipv6 --attachable public
### Para IPV6

Cria rede para os containers:
docker swarm init --advertise-addr  IP_MAQUINA \
docker network create --driver=overlay --attachable public \
docker volume create portainer_data

Crie o diretorio e o arquivo dos certificados para o traefick\
mkdir -p /mnt/data/traefik \
touch /mnt/data/traefik/acme.json \
chmod 600 /mnt/data/traefik/acme.json

Crie o arquivo compose-portainer.yml  segundo o arquivo da raiz do projeto: \
Isso vai instalar o deamon o traefick e o portainer \
docker stack deploy -c compose-portainer.yml portainer 



