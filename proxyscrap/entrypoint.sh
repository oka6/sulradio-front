#!/bin/bash

# Iniciar o Xvfb
Xvfb :99 -screen 0 1024x768x24 &

# Exportar a variável DISPLAY
export DISPLAY=:99

# Iniciar a aplicação Node.js
exec "$@"
