var Nightmare = require('nightmare');
const express = require('express');
const app = express();

vo = require('vo');

function* run(url, type) {
	var nightmare;
	var userAgent;

	switch (type) {
		case 'pc':
			userAgent = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36';
			nightmare = new Nightmare({ show: false });
			yield nightmare.useragent(userAgent)
				.goto(url)
				.wait('body');
			break;

		case 'mobile':
			userAgent = 'Mozilla/5.0 (iPhone; CPU iPhone OS 10_3_1 like Mac OS X) AppleWebKit/603.1.30 (KHTML, like Gecko) Version/10.0 Mobile/14E304 Safari/602.1';
			nightmare = new Nightmare({ show: false, width: 375, height: 667 });
			yield nightmare.useragent(userAgent)
				.goto(url)
				.wait('body');
			break;
	}

	// Get the HTML content of the page
	const html = yield nightmare.evaluate(() => document.documentElement.outerHTML);

	yield nightmare.end();
	return html;
}

app.get('/', function (req, res) {
	let url = req.query.url;
	let type = req.query.type || 'pc'; // Default to 'pc' if type is not provided

	console.log('Request', url, type);

	vo(run(url, type))
		.then(html => res.send(html))
		.catch(error => {
			console.error(error);
			res.status(500).send('Error occurred');
		});
});

app.listen(3000, '0.0.0.0', function () {
	console.log('App listening on port 3000!');
});
