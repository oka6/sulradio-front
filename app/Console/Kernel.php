<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel {
	/**
	 * The Artisan commands provided by your application.
	 *
	 * @var array
	 */
	protected $commands = [
		\Oka6\SulRadio\Console\ProcessXMLAnatel::class,
		\Oka6\SulRadio\Console\ProcessZipDou::class,
		\Oka6\SulRadio\Console\ProcessTicketNotification::class,
		\Oka6\SulRadio\Console\ProcessTicketDeadLines::class,
		\Oka6\SulRadio\Console\ProcessTicketNotificationClient::class,
		\Oka6\SulRadio\Console\ProcessTicketNotificationDaily::class,
		\Oka6\SulRadio\Console\ProcessAtoNotification::class,
		\Oka6\SulRadio\Console\ProcessTrackerUrl::class,
		\Oka6\SulRadio\Console\AdjustFistelEmissoras::class,
		\Oka6\SulRadio\Console\ProcessCleanDou::class,
	];

	/**
	 * Define the application's command schedule.
	 *
	 * @param \Illuminate\Console\Scheduling\Schedule $schedule
	 * @return void
	 */
	protected function schedule( Schedule $schedule ) {
		$pathLog = storage_path("logs/commands-") . date('Y-m-d') . '.log';

		$schedule->command('Sulradio:ProcessZipDou')
			->runInBackground()->withoutOverlapping(720)
			->everyMinute()
			->appendOutputTo($pathLog)
			->when(function () {
				return true;
			});

		$schedule->command('Sulradio:ProcessTrackerUrl')
			->runInBackground()
			->withoutOverlapping(720)
			->everyMinute()
			->appendOutputTo($pathLog)
			->when(function () {
				return true;
			});

		$schedule->command('Sulradio:ProcessTicketNotification')
			->runInBackground()
			->withoutOverlapping(720)
			->hourly()
			->appendOutputTo($pathLog)
			->when(function () {
				return true;
			});

        $schedule->command('Sulradio:ProcessTicketNotificationClient')
            ->runInBackground()
            ->withoutOverlapping(720)
            ->hourlyAt([10, 15, 20, 25, 30, 35 ,40 ,45 ,50 ,55])
            ->appendOutputTo($pathLog)
            ->when(function () {
                return true;
            });

        $schedule->command('Sulradio:ProcessAtoNotification')
			->runInBackground()
			->withoutOverlapping(720)
			->everyFiveMinutes()
			->appendOutputTo($pathLog)
			->when(function () {
				return true;
			});



        $schedule->command('Sulradio:ProcessTicketDeadLines')
			->runInBackground()
			->withoutOverlapping(720)
			->dailyAt('11:00')
			->appendOutputTo($pathLog)
			->when(function () {
				return true;
			});

        $schedule->command('Sulradio:ProcessCleanDou')
			->runInBackground()
			->withoutOverlapping(720)
			->everyMinute()
			->appendOutputTo($pathLog)
			->when(function () {
				return true;
			});

        $schedule->command('backup:clean')
            ->runInBackground()
            ->withoutOverlapping(720)
            ->daily()
            ->at('03:00');

        $schedule->command('backup:run --only-db')
            ->runInBackground()
            ->withoutOverlapping(720)
            ->daily()
            ->at('03:30');

        $schedule->command('backup:monitor')
            ->runInBackground()
            ->withoutOverlapping(720)
            ->daily()
            ->at('04:00');
	}

	/**
	 * Register the commands for the application.
	 *
	 * @return void
	 */
	protected function commands() {
		$this->load(__DIR__ . '/Commands');

		require base_path('routes/console.php');
	}
}
