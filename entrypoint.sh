#!/bin/sh
set -e

cat <<EOF > /var/www/html/.env
APP_NAME=Sulradio
APP_ENV=$APP_ENV
APP_KEY=$APP_KEY
APP_DEBUG=true
APP_LOG_LEVEL=debug
APP_URL=$APP_URL
LOG_CHANNEL=$LOG_CHANNEL
# ...
DB_PORT=$DB_PORT
DB_HOST=$DB_HOST
DB_DATABASE=$DB_DATABASE
DB_USERNAME=$DB_USERNAME
DB_PASSWORD=$DB_PASSWORD
# ...
OKA6_ADMIN_DB_PORT=$OKA6_ADMIN_DB_PORT
OKA6_ADMIN_DB_HOST=$OKA6_ADMIN_DB_HOST
OKA6_ADMIN_DB_NAME=$OKA6_ADMIN_DB_NAME
OKA6_ADMIN_DB_USERNAME=$OKA6_ADMIN_DB_USERNAME
OKA6_ADMIN_DB_PASSWORD=$OKA6_ADMIN_DB_PASSWORD
# ...
OKA6_ADMIN_SESSION_DB_PORT=$OKA6_ADMIN_SESSION_DB_PORT
OKA6_ADMIN_SESSION_DB_HOST=$OKA6_ADMIN_SESSION_DB_HOST
OKA6_ADMIN_SESSION_DB_NAME=$OKA6_ADMIN_SESSION_DB_NAME
OKA6_ADMIN_SESSION_DB_USERNAME=$OKA6_ADMIN_SESSION_DB_USERNAME
OKA6_ADMIN_SESSION_DB_PASSWORD=$OKA6_ADMIN_SESSION_DB_PASSWORD
# ...
SULRADIO_DB_PORT=3306
SULRADIO_DB_HOST=$SULRADIO_DB_HOST
SULRADIO_DB_DATABASE=$SULRADIO_DB_DATABASE
SULRADIO_DB_USERNAME=$SULRADIO_DB_USERNAME
SULRADIO_DB_PASSWORD=$SULRADIO_DB_PASSWORD
# ...
SULRADIO_MONGO_DB_PORT=27017
SULRADIO_MONGO_DB_HOST=$SULRADIO_MONGO_DB_HOST
SULRADIO_MONGO_DB_NAME=$SULRADIO_MONGO_DB_NAME
SULRADIO_MONGO_DB_USERNAME=$SULRADIO_MONGO_DB_USERNAME
SULRADIO_MONGO_DB_PASSWORD=$SULRADIO_MONGO_DB_PASSWORD
# ...
SESSION_DRIVER=mongodb
CACHE_DRIVER=redis
QUEUE_DRIVER=redis
# ...
REDIS_CLIENT=predis
REDIS_HOST=$REDIS_HOST
REDIS_PASSWORD=null
REDIS_PORT=6379
# ...
MAIL_MAILER=$MAIL_MAILER
AWS_ACCESS_KEY_ID=$AWS_ACCESS_KEY_ID
AWS_SECRET_ACCESS_KEY=$AWS_SECRET_ACCESS_KEY
AWS_DEFAULT_REGION=$AWS_DEFAULT_REGION
MAIL_HOST=$MAIL_HOST
MAIL_PORT=$MAIL_PORT
MAIL_USERNAME=$MAIL_USERNAME
MAIL_PASSWORD=$MAIL_PASSWORD
MAIL_ENCRYPTION=$MAIL_ENCRYPTION
MAIL_FROM_NAME=$MAIL_FROM_NAME
MAIL_FROM_ADDRESS=$MAIL_FROM_ADDRESS
# ...
OKA6_ADMIN_FAVICON="/vendor/sulradio/img/favicon/sulradio.png"
OKA6_ADMIN_BACKGROUND_LOGIN="#19202a"
OKA6_ADMIN_LOGO="/vendor/sulradio/img/logos/sulradio.png"
OKA6_ADMIN_LOGO_LABEL="Sulradio"
OKA6_ADMIN_HIDE_FOOTER=true
# ...
DO_SPACES_KEY=$DO_SPACES_KEY
DO_SPACES_SECRET=$DO_SPACES_SECRET
DO_SPACES_REGION=$DO_SPACES_REGION
DO_SPACES_ENDPOINT=$DO_SPACES_ENDPOINT
DO_SPACES_URL=$DO_SPACES_URL
DO_SPACES_BUCKET=$DO_SPACES_BUCKET
EOF

service cron restart
# Define o comando a ser adicionado
CRON_JOB="* * * * *  su -s /bin/sh www-data -c 'php /var/www/html/artisan schedule:run >> /dev/null 2>&1'"
# Verifica se o crontab já contém o comando
if ! crontab -l | grep -Fq "$CRON_JOB"; then
    # Adiciona o cron job se ele não existir
    (crontab -l 2>/dev/null; echo "$CRON_JOB") | crontab -
    (crontab -l 2>/dev/null; echo "$CRON_JOB") | crontab -
    echo "Cron job adicionado."
else
    echo "Cron job já existe"
fi

log_error() {
    echo "ERRO: $1" >&2
}

# Ajusta as permissões do diretório
chown -R www-data:www-data /var/www/html || { log_error "Falha ao ajustar permissões"; true; }
# Navega para o diretório do projeto e instala dependências
cd /var/www/html && composer install --no-dev --optimize-autoloader || { log_error "Falha na instalação do Composer"; true; }
# Executa as migrações e publica os assets
su -s /bin/sh www-data -c "php artisan migrate --database=oka6_admin --force && php artisan vendor:publish --tag=public" || { log_error "Falha nas migrações ou publicação de assets"; true; }
# Otimiza a aplicação
su -s /bin/sh www-data -c "php artisan optimize" || { log_error "Falha na otimização da aplicação"; true; }

"$@"