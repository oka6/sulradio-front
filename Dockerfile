# Usar uma imagem base do Ubuntu
FROM --platform=linux/amd64 ubuntu:22.04

# Evitar interações durante a instalação de pacotes
ARG DEBIAN_FRONTEND=noninteractive

# Atualizar o cache do apt e instalar pacotes
RUN apt-get update && apt-get install -y \
    software-properties-common \
    && add-apt-repository ppa:ondrej/php \
    && apt-get update \
    && apt-get install -y \
    php8.0 \
    php8.0-cli \
    php8.0-fpm \
    php8.0-mbstring \
    php8.0-xml \
    php8.0-curl \
    php8.0-mongodb \
    php8.0-zip \
    php8.0-bcmath \
    php8.0-gd \
    php8.0-mysql \
    curl \
    nginx \
    git \
    vim \
    cron \
    gzip \
    mysql-client \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

# Instalar o Composer globalmente
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

#Instalar mongo client
RUN curl -fsSL https://pgp.mongodb.com/server-7.0.asc | gpg -o /usr/share/keyrings/mongodb-server-7.0.gpg --dearmor && \
    echo "deb [ arch=amd64,arm64 signed-by=/usr/share/keyrings/mongodb-server-7.0.gpg ] https://repo.mongodb.org/apt/ubuntu jammy/mongodb-org/7.0 multiverse" | tee /etc/apt/sources.list.d/mongodb-org-7.0.list && \
    apt-get update && \
    apt-get install  -y mongodb-org-tools

# Copiar as configurações do Nginx e PHP (se necessário)
COPY ./default.conf /etc/nginx/sites-available/default
COPY ./php.ini /etc/php/8.0/cli/php.ini
COPY ./php.ini /etc/php/8.0/fpm/php.ini

# Definir o diretório de trabalho
WORKDIR /var/www/html

# Limpar o diretório e clonar o projeto Git
RUN rm -rf /var/www/html/*  \
    && git clone https://x-token-auth:ATCTT3xFfGN0Iju2jLsW5R37A2TDDiEuOJfC3JCGdPxotwL51_cYbpB4dYDNFznI78gXRKgjireVsRSKnCL5KjfFzlrb5o8Gzsr41VAZZ477F90g7Pw3TWHSrXcYCV1xJ6gQX-Kx0xaWWH4dEUDsJj-sCsEx15OzRXwcRRcuiabLlpZIwzenF-U=EC0711FD@bitbucket.org/oka6/sulradio-front.git /var/www/html

# Expor a porta 80
EXPOSE 80
# Copiar o script de entrada para o container
COPY entrypoint.sh /entrypoint.sh

# Tornar o script de entrada executável
RUN chmod +x /entrypoint.sh

# Definir o script de entrada como ponto de entrada
ENTRYPOINT ["/entrypoint.sh"]

STOPSIGNAL SIGTERM
# Startup command
CMD ["sh", "-c", "service php8.0-fpm start && nginx -g 'daemon off;'"]